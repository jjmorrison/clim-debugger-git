;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: clim-debugger; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2013.
;;; 

;;;
;;; Licensed as McCLIM
;;;

(in-package :clim-debugger)

(defparameter *saved-debugger-hooks* nil)

(defun enable-clim-debugger ()
  (push *debugger-hook* *saved-debugger-hooks*)
  (setf *debugger-hook* #'debugger))

(defun disable-clim-debugger ()
  (setf *debugger-hook* (pop *saved-debugger-hooks*)))

(export
 '(enable-clim-debugger
   disable-clim-debugger))
